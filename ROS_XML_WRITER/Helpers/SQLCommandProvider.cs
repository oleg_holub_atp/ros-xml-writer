﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using ROS_XML_WRITER.Enums;

namespace ROS_XML_WRITER.Helpers
{
    public class SQLCommandProvider
    {

        public static string GetInvoiceCommand(VAT_TRANSACTIONS_TYPE commandType)
        {
            string command = String.Empty;
            string commandPath = GetCommandSource(commandType);
            if (File.Exists(commandPath))
            {
                command = File.ReadAllText(commandPath);
            }
            return command;
        }

        private static string GetCommandSource(VAT_TRANSACTIONS_TYPE commandType)
        {
            string path = System.Reflection.Assembly.GetAssembly(typeof(SQLCommandProvider)).Location;
            string PathToClassLibPro = Path.GetDirectoryName(path);
            switch (commandType)
            {
                case VAT_TRANSACTIONS_TYPE.PURCHASE_VAT:
                    return String.Format(@"{0}\{1}\{2}", PathToClassLibPro, "SQL", "VatOnPurchase.txt");
                case VAT_TRANSACTIONS_TYPE.SALES_VAT:
                    return String.Format(@"{0}\{1}\{2}", PathToClassLibPro, "SQL", "VatOnSales.txt"); //VatOnSales
                case VAT_TRANSACTIONS_TYPE.SALES_TO_EU:
                    return String.Format(@"{0}\{1}\{2}", PathToClassLibPro, "SQL", "SalesToEu.txt"); //SalesToEu
                case VAT_TRANSACTIONS_TYPE.SALES_FROM_EU:
                    return String.Format(@"{0}\{1}\{2}", PathToClassLibPro, "SQL", "SalesFromEu.txt");//SalesFromEu
                default:
                    return String.Empty;
            }
        }
    }
}
