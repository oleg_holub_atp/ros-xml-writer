﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace ROS_XML_WRITER.Helpers
{
    public class ConfigHelper
    {
        public static string GetCompanyVatName(string company)
        {
            switch (company)
            {
                case "atpn":
                    return ConfigurationManager.AppSettings["atpCompanyName"];
                case "purus":
                    return ConfigurationManager.AppSettings["purusCompanyName"];
                case "ca":
                    return ConfigurationManager.AppSettings["caCompanyName"];
                default:

                    return ConfigurationManager.AppSettings["atpCompanyName"];
            }
        }

        public static string GetCompanyVatEmail(string company)
        {
            switch (company)
            {
                case "atpn":
                    return ConfigurationManager.AppSettings["atpContactEmail"];
                case "purus":
                    return ConfigurationManager.AppSettings["purusContactEmail"];
                case "ca":
                    return ConfigurationManager.AppSettings["caContactEmail"];
                default:

                    return ConfigurationManager.AppSettings["atpContactEmail"];
            }
        }

        public static string GetCompanyVatNo(string company)
        {
            switch (company)
            {
                case "atpn":
                    return ConfigurationManager.AppSettings["atpVatNo"];
                case "purus":
                    return ConfigurationManager.AppSettings["purusVatNo"];
                case "ca":
                    return ConfigurationManager.AppSettings["caVatNo"];
                default:

                    return ConfigurationManager.AppSettings["atpVatNo"];
            }
        }
    }
}
