﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;

namespace ROS_XML_Writer.Helpers
{
    public class VatHolder
    {
        private string vatSales; public string VATRSALES { get { return vatSales; } set { vatSales = value; } }
        private string vatPruch; public string VATPURCH { get { return vatPruch; } set { vatPruch = value; } }
        private string salesToEu; public string SALESTOEU { get { return salesToEu; } set { salesToEu = value; } }
        private string salesFromEu; public string SALESFROMEU { get { return salesFromEu; } set { salesFromEu = value; } }
    }
}
