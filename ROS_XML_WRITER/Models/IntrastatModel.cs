﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROS_XML_WRITER.Models
{
    public class IntrastatModel
    {
        public string CountryCode { get; set; }
        public string CommodityCode { get; set; }
        public int ShippedQty { get; set; }
        public string SupplyUnit { get; set; }
        public string Quantity { get; set; }
        public int Value { get; set; }
        public int Weight { get; set; }
    }
}
