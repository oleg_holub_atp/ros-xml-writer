﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROS_XML_WRITER.Models
{
    public class VatTransaction
    {
        public string InvoiceNumber { get; set; }
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public string InvoiceDate { get; set; }
        public double InvoiceValue { get; set; }
        public double InvoiceVat { get; set; }
        public double InvoiceTotal { get; set; }
    }
}
