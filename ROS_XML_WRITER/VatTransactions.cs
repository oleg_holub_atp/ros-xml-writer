﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ROS_XML_WRITER.Enums;
using ROS_XML_WRITER.Models;
using ROS_XML_Writer.DataProvider;
using System.IO;

namespace ROS_XML_WRITER
{
    public partial class VatTransactions : Form
    {
        private string company;
        private string startdate;
        private string enddate;

        public VatTransactions(string company, string startdate, string enddate)
        {
            InitializeComponent();
            this.company = company;
            this.startdate = startdate;
            this.enddate = enddate;
        }

        private void VatTransactions_Load(object sender, EventArgs e)
        {
            gvTransactions.DataSource = getTransactions(VAT_TRANSACTIONS_TYPE.SALES_VAT);
            gvTransactions.BestFitColumns();
        }

        private void RadioButton_ChackedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
            {
                rb.Font = new Font(this.Font, FontStyle.Bold);
                if (rb.Name == rbSalesToEu.Name)
                {
                    gvTransactions.DataSource = getTransactions(VAT_TRANSACTIONS_TYPE.SALES_TO_EU);
                }
                else if (rb.Name == rbSalesFromEu.Name)
                {
                    gvTransactions.DataSource = getTransactions(VAT_TRANSACTIONS_TYPE.SALES_FROM_EU);
                }
                else if (rb.Name == rbVatOnPurchase.Name)
                {
                    gvTransactions.DataSource = getTransactions(VAT_TRANSACTIONS_TYPE.PURCHASE_VAT);
                }
                else if (rb.Name == rbVatOnSales.Name)
                {
                    gvTransactions.DataSource = getTransactions(VAT_TRANSACTIONS_TYPE.SALES_VAT);
                }
                else
                {
                }
                gvTransactions.BestFitColumns();
            }
            else
                rb.Font = new Font(this.Font, FontStyle.Regular);
            

            // TODO: Change Grid Data source
        }

        private List<VatTransaction> getTransactions(VAT_TRANSACTIONS_TYPE type)
        {
            DataProvider provider = new DataProvider(company);
            return provider.getTransactions(type, startdate, enddate);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "CSV (*.csv)|*.csv";
            sfd.FileName = GetExportFileName();
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                //ToCsV(dataGridView1, @"c:\export.xls");
                SaveToExcel(sfd.FileName); // Here dataGridview1 is your grid view name 
            } 
        }

        private void SaveToExcel(string fileName)
        {
            StringBuilder output = new StringBuilder();
            StringBuilder headers = new StringBuilder();
            for (int i = 0; i < gvTransactions.ColumnCount; i++)
            {
                headers.Append(gvTransactions.Columns[i].HeaderText + ",");
            }
            output.Append(headers.ToString() + Environment.NewLine);

            for (int i = 0; i < gvTransactions.RowCount; i++)
            {
                StringBuilder line = new StringBuilder();
                for (int j = 0; j < gvTransactions.ColumnCount; j++)
                {
                    line.Append(gvTransactions.Rows[i].Cells[j].Value + ",");
                }
                output.Append(line.ToString() + Environment.NewLine);
            }

            Encoding utf16 = Encoding.GetEncoding(1254);
            byte[] toSave = utf16.GetBytes(output.ToString());
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(toSave, 0, output.Length); //write the encoded file
            bw.Flush();
            bw.Close();
            fs.Close();
        }

        private string GetExportFileName()
        {
            string filename = String.Empty;
            if (rbSalesFromEu.Checked)
                filename = "SalesFromEu_";
            if (rbSalesToEu.Checked)
                filename = "SaleToEu_";
            if (rbVatOnSales.Checked)
                filename = "VatOnSales_";
            if (rbVatOnPurchase.Checked)
                filename = "VatOnPurchase_";

            return filename + DateTime.Now.ToString("yyyyMMdd");
        }
    }
}
