﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ROS_XML_WRITER.Enums
{
    public enum VAT_TRANSACTIONS_TYPE
    {
        SALES_VAT,
        PURCHASE_VAT,
        SALES_TO_EU,
        SALES_FROM_EU
    }
}
