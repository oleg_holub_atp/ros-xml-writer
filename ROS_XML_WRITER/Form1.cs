﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Microsoft.VisualBasic;
using ROS_XML_Writer.DataProvider;
using ROS_XML_Writer.Helpers;
using Telerik.WinControls.UI;
using System.Configuration;
using ROS_XML_WRITER;
using ROS_XML_WRITER.Helpers;

namespace ROS_XML_Writer
{
    public partial class MainForm : Form
    {
        string compName = "atp";
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            tabSelection.Width = this.Width;
            tab1.Width= this.Width;
            tab2.Width = this.Width;
            tab3.Width = this.Width;
            tabSelection.SelectedPage = tab1;
            radDateTimePicker1.Value = DateTime.Now;
            radDateTimePicker2.Value = DateTime.Now;
            rdpINT_From.Value = DateTime.Now;
            rdpINT_To.Value = DateTime.Now;
            rdpVIES_From.Value = DateTime.Now;
            rdpVIES_To.Value = DateTime.Now;
            setPagerHeader();
        }
        #region VAT3 RETURN
        private void radButton1_Click(object sender, EventArgs e)
        {
            if (rcbTAX_Freq.Text.Length < 3)
            {
                MessageBox.Show("Please select report frequancy value");
                return;
            }
            if (rcbVAT_Type.Text.Length < 3)
            {
                MessageBox.Show("Please select report type value");
                return;
            }
            ROS_XML_Writer.DataProvider.DataProvider data = new ROS_XML_Writer.DataProvider.DataProvider(compName);
            VatHolder vh = data.SalesVAT(1, radDateTimePicker1.Value.ToShortDateString(), radDateTimePicker2.Value.ToShortDateString());
            if (vh == null) return;
            if (vh.VATPURCH == "") vh.VATPURCH = "0";
            if (vh.VATRSALES == "") vh.VATRSALES = "0";
            if (vh.SALESTOEU == "") vh.SALESTOEU = "0";
            if (vh.SALESFROMEU == "") vh.SALESFROMEU = "0";
            rtbVAT_Sales.Text= vh.VATRSALES.ToString();
            rtbVAT_Purchs.Text =  vh.VATPURCH.ToString();
            rtbVAT_ToEU.Text =  vh.SALESTOEU.ToString();
            rtbVAT_FromEU.Text =  vh.SALESFROMEU.ToString();
            rlDifference.Text =  (Int32.Parse(vh.VATRSALES) - Int32.Parse(vh.VATPURCH)).ToString();
            rpSales.Visible = true;
            rbVAT_Create.Visible = true;
            // create VAt holder class for purchases and asign it to rgPurchases
        }

        private void rbVAT_Create_Click(object sender, EventArgs e)
        {
            string filename = String.Format("C:\\Temp\\VATRETURN_{0}.xml", compName);
            if (System.IO.File.Exists(filename))
            {
                System.IO.File.Delete(filename);

            }
            try
            {
                XmlTextWriter xmlWriter = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                xmlWriter.Formatting = Formatting.Indented;
                xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                xmlWriter.WriteStartElement("VAT3");
                xmlWriter.WriteAttributeString("name", ConfigHelper.GetCompanyVatName(compName));
                xmlWriter.WriteAttributeString("regnum", ConfigHelper.GetCompanyVatNo(compName));
                string freq = "";
                switch (rcbTAX_Freq.Text)
                {
                    case "Bi-Monthly/Repayment Only": freq = "0"; break;
                    case "Bi-Annual": freq = "1"; break;
                }
                xmlWriter.WriteAttributeString("filefreq", freq);
                xmlWriter.WriteAttributeString("startdate", radDateTimePicker1.Value.ToShortDateString());
                xmlWriter.WriteAttributeString("enddate", radDateTimePicker2.Value.ToShortDateString());
                string type = "";
                switch (rcbVAT_Type.Text)
                {
                    case "Original VAT3 Return": type = "0"; break;
                    case "Supplementary VAT3 Return": type = "1"; break;
                    case "Amended VAT3 Return": type = "2"; break;
                }
                xmlWriter.WriteAttributeString("type", type);
                xmlWriter.WriteAttributeString("sales", rtbVAT_Sales.Text);
                xmlWriter.WriteAttributeString("purchs", rtbVAT_Purchs.Text);
                xmlWriter.WriteAttributeString("goodsto", rtbVAT_ToEU.Text);
                xmlWriter.WriteAttributeString("goodsfrom", rtbVAT_FromEU.Text);
                xmlWriter.WriteAttributeString("formversion", "1");
                xmlWriter.WriteAttributeString("language", "E");
                xmlWriter.WriteAttributeString("currency", "E");
                xmlWriter.Close();
                MessageBox.Show("XML file created successfully and saved to " + filename + ".");
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error: " + exp.Message + ".Please try again later");
            }
        }

        private void radDateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            rpSales.Visible = false;
            rbVAT_Create.Visible = false;
            rcbTAX_Freq.Text = "";
            rcbVAT_Type.Text = "";
        }

        private void radDateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            rpSales.Visible = false;
            rbVAT_Create.Visible = false;
            rcbTAX_Freq.Text = "";
            rcbVAT_Type.Text = "";
        }
        #endregion

        #region INTRASTAT
        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            rgvINT_Results.DataSource = null;
            rgvINT_Results.Visible = false;
            rbINT_send.Visible = false;
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
            {
                rb.ForeColor = Color.Red;
            }
            else
            {
                rb.ForeColor = Color.Black;
            }
        }
        private void rbINT_Go_Click(object sender, EventArgs e)
        {
            try
            {
                ROS_XML_Writer.DataProvider.DataProvider data = new ROS_XML_Writer.DataProvider.DataProvider(compName);
                if (rbArrival.Checked)
                {

                    rgvINT_Results.DataSource = data.IntrastatReportData(rdpINT_From.Value.ToShortDateString(), rdpINT_To.Value.ToShortDateString(), "A");
                    foreach (GridViewDataColumn column in rgvINT_Results.Columns)
                    {
                        column.BestFit();
                    }
                    rpINT_Results.Visible = true;
                    rgvINT_Results.Visible = true;
                    rbINT_send.Visible = true;
                }
                if (rbDispatch.Checked)
                {
                    rgvINT_Results.DataSource = data.IntrastatReportData(rdpINT_From.Value.ToShortDateString(), rdpINT_To.Value.ToShortDateString(), "D");
                    foreach (GridViewDataColumn column in rgvINT_Results.Columns)
                    {
                        column.BestFit();
                    }
                    rpINT_Results.Visible = true;
                    rgvINT_Results.Visible = true;
                    rbINT_send.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR:" + ex.Message);
            }          
        }

        private void rdpINT_From_ValueChanged(object sender, EventArgs e)
        {
            rbINT_send.Visible = false;
            rpINT_Results.Visible = false;
        }

        private void rdpINT_To_ValueChanged(object sender, EventArgs e)
        {
            rbINT_send.Visible = false;
            rpINT_Results.Visible = false;
        }

        private void rbINT_send_Click(object sender, EventArgs e)
        {
            #region Arrivals
            if (rbArrival.Checked)
            {
                string filename = "C:\\Temp\\ATP_Istat_Import.xml";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.File.Delete(filename);

                }
                try
                {
                    XmlTextWriter xmlWriter = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                    xmlWriter.Formatting = Formatting.Indented;
                    xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                    xmlWriter.WriteStartElement("Istat");
                    xmlWriter.WriteAttributeString("formversion", "1");
                    xmlWriter.WriteAttributeString("language", "E");
                    xmlWriter.WriteAttributeString("periodstart", rdpINT_From.Value.ToShortDateString());
                    xmlWriter.WriteAttributeString("periodend", rdpINT_To.Value.ToShortDateString());
                    xmlWriter.WriteStartElement("Return");
                    xmlWriter.WriteAttributeString("flow", "A");
                    if (rgvINT_Results.Rows.Count > 0)
                    {
                        xmlWriter.WriteAttributeString("returnType", "1");
                    }
                    else
                    {
                        xmlWriter.WriteAttributeString("returnType", "0");
                    }
                    xmlWriter.WriteAttributeString("statPeriod", rdpINT_To.Value.ToShortDateString().Replace("/", "").Substring(2));
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("Party");
                    xmlWriter.WriteAttributeString("partyType", "PSI");
                    xmlWriter.WriteAttributeString("vatNum", ConfigHelper.GetCompanyVatNo(compName));
                    xmlWriter.WriteAttributeString("name", ConfigHelper.GetCompanyVatName(compName));
                    xmlWriter.WriteAttributeString("address1", "Corcanree Business Park");
                    xmlWriter.WriteAttributeString("address2", "Dock Road");
                    xmlWriter.WriteAttributeString("address3", "Limerick");
                    xmlWriter.WriteAttributeString("contact", ConfigurationManager.AppSettings["contactPerson"]);
                    xmlWriter.WriteAttributeString("phone", "061496600");
                    xmlWriter.WriteAttributeString("statsValueRequired", "N");
                    xmlWriter.WriteAttributeString("email", ConfigHelper.GetCompanyVatEmail(compName));
                    xmlWriter.WriteAttributeString("arrivals", "1");
                    xmlWriter.WriteAttributeString("dispatch", "1");
                    //xmlWriter.WriteAttributeString("statsValueRequired", "N");
                    xmlWriter.WriteEndElement();
                    //add items XML here
                    if (rgvINT_Results.Rows.Count > 0)
                    {
                        foreach (GridViewRowInfo row in rgvINT_Results.Rows)
                        {
                            xmlWriter.WriteStartElement("Item");
                            xmlWriter.WriteStartElement("ItemDetails");
                            xmlWriter.WriteAttributeString("ctryConsignDestn", row.Cells[0].Value.ToString());
                            xmlWriter.WriteAttributeString("ctryOrigin", row.Cells[0].Value.ToString());
                            xmlWriter.WriteAttributeString("transaction", "1");
                            xmlWriter.WriteAttributeString("transport", "3");
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("CN8");
                            xmlWriter.WriteAttributeString("code", row.Cells[1].Value.ToString().Substring(0, 8));
                            xmlWriter.WriteAttributeString("netMass", row.Cells[6].Value.ToString());
                            xmlWriter.WriteAttributeString("netMassOpt", "M");
                            if (row.Cells[3].Value.ToString().Length != 0) xmlWriter.WriteAttributeString("suCode", row.Cells[3].Value.ToString());
                            else xmlWriter.WriteAttributeString("suCode", "0");
                            xmlWriter.WriteAttributeString("suQuanity", (Int32.Parse(row.Cells[2].Value.ToString()) * Int32.Parse(row.Cells[4].Value.ToString())).ToString());
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("ItemValues");
                            xmlWriter.WriteAttributeString("invEuroValue", row.Cells[5].Value.ToString());
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteEndElement();
                        }
                    }

                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                    MessageBox.Show("Intrastat Import XML file created successfully and saved to " + filename + ".");
                }
                catch (Exception exp)
                {
                    MessageBox.Show("Error: " + exp.Message + ".Please try again later");
                }
            }
            
            
            #endregion

            #region Dispatch
            if (rbDispatch.Checked)
            {
                string filename = "C:\\Temp\\ATP_Istat_Export.xml";
                if (System.IO.File.Exists(filename))
                {
                    System.IO.File.Delete(filename);

                }
                try
                {
                    XmlTextWriter xmlWriter = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                    xmlWriter.Formatting = Formatting.Indented;
                    xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                    xmlWriter.WriteStartElement("Istat");
                    xmlWriter.WriteAttributeString("formversion", "1");
                    xmlWriter.WriteAttributeString("language", "E");
                    xmlWriter.WriteAttributeString("periodstart", rdpINT_From.Value.ToShortDateString());
                    xmlWriter.WriteAttributeString("periodend", rdpINT_To.Value.ToShortDateString());
                    xmlWriter.WriteStartElement("Return");
                    xmlWriter.WriteAttributeString("flow", "D");
                    if (rgvINT_Results.Rows.Count > 0)
                    {
                        xmlWriter.WriteAttributeString("returnType", "1");
                    }
                    else
                    {
                        xmlWriter.WriteAttributeString("returnType", "0");
                    }
                    xmlWriter.WriteAttributeString("statPeriod", rdpINT_To.Value.ToShortDateString().Replace("/", "").Substring(2));
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("Party");
                    xmlWriter.WriteAttributeString("partyType", "PSI");
                    xmlWriter.WriteAttributeString("vatNum", ConfigHelper.GetCompanyVatNo(compName));
                    xmlWriter.WriteAttributeString("name", ConfigHelper.GetCompanyVatName(compName));
                    xmlWriter.WriteAttributeString("address1", "Corcanree Business Park");
                    xmlWriter.WriteAttributeString("address2", "Dock Road");
                    xmlWriter.WriteAttributeString("address3", "Limerick");
                    xmlWriter.WriteAttributeString("contact", ConfigurationManager.AppSettings["contactPerson"]);
                    xmlWriter.WriteAttributeString("phone", "061496600");
                    xmlWriter.WriteAttributeString("statsValueRequired", "N");
                    xmlWriter.WriteAttributeString("email", ConfigHelper.GetCompanyVatEmail(compName));
                    xmlWriter.WriteAttributeString("arrivals", "1");
                    xmlWriter.WriteAttributeString("dispatch", "1");
                    //xmlWriter.WriteAttributeString("statsValueRequired", "N");
                    xmlWriter.WriteEndElement();
                    //add items XML here
                    if (rgvINT_Results.Rows.Count > 0)
                    {
                        foreach (GridViewRowInfo row in rgvINT_Results.Rows)
                        {
                            xmlWriter.WriteStartElement("Item");
                            xmlWriter.WriteStartElement("ItemDetails");
                            xmlWriter.WriteAttributeString("ctryConsignDestn", row.Cells[0].Value.ToString());
                            xmlWriter.WriteAttributeString("ctryOrigin", row.Cells[0].Value.ToString());
                            xmlWriter.WriteAttributeString("transaction", "1");
                            xmlWriter.WriteAttributeString("transport", "3");
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("CN8");
                            //xmlWriter.WriteAttributeString("code", row.Cells[1].Value.ToString().Substring(0, 8)); // this is where the commodity code get truncated
                            xmlWriter.WriteAttributeString("code", row.Cells[1].Value.ToString());
                            xmlWriter.WriteAttributeString("netMass", row.Cells[6].Value.ToString());
                            xmlWriter.WriteAttributeString("netMassOpt", "M");
                            if (row.Cells[3].Value.ToString().Length != 0) xmlWriter.WriteAttributeString("suCode", row.Cells[3].Value.ToString());
                            else xmlWriter.WriteAttributeString("suCode", "0");
                            xmlWriter.WriteAttributeString("suQuanity", (Int32.Parse(row.Cells[2].Value.ToString()) * Int32.Parse(row.Cells[4].Value.ToString())).ToString());
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("ItemValues");
                            xmlWriter.WriteAttributeString("invEuroValue", row.Cells[5].Value.ToString());
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteEndElement();
                        }
                    }

                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                    MessageBox.Show("Intrastat Export XML file created successfully and saved to " + filename + ".");
                }
                catch (Exception exp)
                {
                    MessageBox.Show("Error: " + exp.Message + ".Please try again later");
                }
            }

            #endregion
        }

        #endregion

        #region VIES
        private void rdpVIES_From_ValueChanged(object sender, EventArgs e)
        {
            rbVIES_Create.Visible = false;
            rpVIES_Results.Visible = false;
        }
        private void rdpVIES_To_ValueChanged(object sender, EventArgs e)
        {
            rbVIES_Create.Visible = false;
            rpVIES_Results.Visible = false;
        }
        private void rbVIES_Go_Click(object sender, EventArgs e)
        {
            if (rcbVIES_type.Text.Length < 3)
            {
                MessageBox.Show("Please select VIES Type");
                return;
            }
            ROS_XML_Writer.DataProvider.DataProvider data = new ROS_XML_Writer.DataProvider.DataProvider(compName);
            rgvVIES_Results.DataSource = data.ViesReportData(rdpVIES_From.Value.ToShortDateString(), rdpVIES_To.Value.ToShortDateString());
            foreach (GridViewDataColumn column in rgvVIES_Results.Columns)
            {
                column.BestFit();
            }
            rbVIES_Create.Visible = true;
            rpVIES_Results.Visible = true;
        }

        private void rbVIES_Create_Click(object sender, EventArgs e)
        {
            string filename = "C:\\Temp\\ATP_VIES.xml";
            if (System.IO.File.Exists(filename))
            {
                System.IO.File.Delete(filename);

            }
            try
            {
                XmlTextWriter xmlWriter = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                xmlWriter.Formatting = Formatting.Indented;
                xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                xmlWriter.WriteStartElement("ViesFile");
                xmlWriter.WriteAttributeString("formversion", "1");
                xmlWriter.WriteAttributeString("periodstart", rdpVIES_From.Value.ToShortDateString());
                xmlWriter.WriteAttributeString("periodend", rdpVIES_To.Value.ToShortDateString());
                xmlWriter.WriteAttributeString("language", "E");
                xmlWriter.WriteStartElement("HeaderDetails");
                xmlWriter.WriteStartElement("Header");
                xmlWriter.WriteAttributeString("thirdparty", "false");
                xmlWriter.WriteAttributeString("tradernumber", ConfigHelper.GetCompanyVatNo(compName));
                xmlWriter.WriteAttributeString("returntype", rcbVIES_type.Text);
                xmlWriter.WriteAttributeString("period", ViesPeriod(rdpVIES_From.Value.ToShortDateString(), rdpVIES_To.Value.ToShortDateString()));
                if (rgvVIES_Results.Rows.Count == 0)
                {
                    xmlWriter.WriteAttributeString("nilreturn", "True");
                    xmlWriter.WriteEndElement();
                }
                else
                {
                    xmlWriter.WriteAttributeString("nilreturn", "False");
                    xmlWriter.WriteEndElement();
                    foreach (GridViewRowInfo row in rgvVIES_Results.Rows)
                    {
                        xmlWriter.WriteStartElement("LineItemDetails");
                        xmlWriter.WriteStartElement("Details");
                        xmlWriter.WriteAttributeString("custnumber", row.Cells[0].Value.ToString());
                        xmlWriter.WriteAttributeString("triang", "False");
                        xmlWriter.WriteAttributeString("valsupplies", row.Cells[2].Value.ToString());
                        if (row.Cells[1].Value.ToString() == "R")
                        {
                            xmlWriter.WriteAttributeString("services", "False");
                        }
                        else
                        {
                            xmlWriter.WriteAttributeString("services", "True");
                        }
                        xmlWriter.WriteEndElement();
                        xmlWriter.WriteEndElement();
                    }
                }
                xmlWriter.WriteEndElement();
                xmlWriter.Close();
                MessageBox.Show("XML file created successfully and saved to " + filename + ".");
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error: " + exp.Message + ".Please try again later");
            }
        }
        
        public string ViesPeriod(string startdate, string enddate)
        {
            string period = startdate.Substring(0,2);
            switch (startdate.Replace("/", "").Substring(2, 2))
            {
                case "01": period = period + " Jan "; break;
                case "02": period = period + " Feb "; break;
                case "03": period = period + " Mar "; break;
                case "04": period = period + " Apr "; break;
                case "05": period = period + " May "; break;
                case "06": period = period + " Jun "; break;
                case "07": period = period + " Jul "; break;
                case "08": period = period + " Aug "; break;
                case "09": period = period + " Sep "; break;
                case "10": period = period + " Oct "; break;
                case "11": period = period + " Nov "; break;
                case "12": period = period + " Dec "; break;
            }
            period += startdate.Replace("/", "").Substring(6) + " - " + enddate.Substring(0, 2); 
            switch (enddate.Replace("/", "").Substring(2, 2))
            {
                case "01": period = period + " Jan "; break;
                case "02": period = period + " Feb "; break;
                case "03": period = period + " Mar "; break;
                case "04": period = period + " Apr "; break;
                case "05": period = period + " May "; break;
                case "06": period = period + " Jun "; break;
                case "07": period = period + " Jul "; break;
                case "08": period = period + " Aug "; break;
                case "09": period = period + " Sep "; break;
                case "10": period = period + " Oct "; break;
                case "11": period = period + " Nov "; break;
                case "12": period = period + " Dec "; break;
            }
            period += enddate.Replace("/", "").Substring(6);
            return period;
        }
        #endregion

        private void radMenuItem_Click(object sender, EventArgs e)
        {
            if(((RadMenuItem)sender).Tag.ToString() =="3")
            {
                compName = "purus";
            }
            else if (((RadMenuItem)sender).Tag.ToString() == "2")
            {
                compName = "ca";
            }
            else
            {
                compName = "atp";
            }
            setPagerHeader();
            resetCompanyControls();
            setVatReturnDetails(compName);
        }

        private void setVatReturnDetails(string compName)
        {
            switch (compName)
            {
                case "atp":
                    lblVat3CompanyName.Text = ConfigurationManager.AppSettings["atpCompanyName"];
                    lblVat3Account.Text = ConfigurationManager.AppSettings["atpVatNo"];
                    lblVat3BankAccount.Text = ConfigurationManager.AppSettings["atpBankAccount"];
                    lblVat3SortCode.Text = ConfigurationManager.AppSettings["atpSortCode"];
                    break;
                case "ca":
                    lblVat3CompanyName.Text = ConfigurationManager.AppSettings["caCompanyName"];
                    lblVat3Account.Text = ConfigurationManager.AppSettings["caVatNo"];
                    lblVat3BankAccount.Text = ConfigurationManager.AppSettings["caBankAccount"];
                    lblVat3SortCode.Text = ConfigurationManager.AppSettings["caSortCode"];
                    break;

                case "purus":
                    lblVat3CompanyName.Text = ConfigurationManager.AppSettings["purusCompanyName"];
                    lblVat3Account.Text = ConfigurationManager.AppSettings["purusVatNo"];
                    lblVat3BankAccount.Text = ConfigurationManager.AppSettings["purusBankAccount"];
                    lblVat3SortCode.Text = ConfigurationManager.AppSettings["purusSortCode"];
                    break;
                default:

                    break;

            }
        }
        
        private void setPagerHeader()
        {
            string company = "";
            switch (compName)
            {
                case "atp":
                    company = " - ATP";
                    break;
                case "ca":
                    company = " - Cosmetic Alliance";
                    break;

                case "purus":
                    company = " - Purus";
                    break;
                default:

                    break;

            }
            tabSelection.Pages[0].Text = "VAT RETURN" + company;
            tabSelection.Pages[1].Text = "INTRASTAT" + company;
            tabSelection.Pages[2].Text = "VIES" + company;
        }

        private void resetCompanyControls()
        {
            rpSales.Visible = false;
            rbVAT_Create.Visible = false;
            rbVIES_Create.Visible = false;
            rpVIES_Results.Visible = false;
            rpINT_Results.Visible = false;
            rbINT_send.Visible = false;
        }

        private void btnViewVatTransactions_Click(object sender, EventArgs e)
        {
            VatTransactions frmTransactions = new VatTransactions(compName, radDateTimePicker1.Value.ToString("yyyy-MM-dd"), radDateTimePicker2.Value.ToString("yyyy-MM-dd"));
            frmTransactions.ShowDialog();
        }

    }
}
