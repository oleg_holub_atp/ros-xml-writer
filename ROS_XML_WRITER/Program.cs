﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace ROS_XML_WRITER
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ROS_XML_Writer.MainForm());
        }
    }
}
