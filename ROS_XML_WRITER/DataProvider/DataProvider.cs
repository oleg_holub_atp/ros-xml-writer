﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using ROS_XML_Writer.Helpers;
using System.Data;
using ROS_XML_WRITER.Models;
using ROS_XML_WRITER.Helpers;
using ROS_XML_WRITER.Enums;

namespace ROS_XML_Writer.DataProvider
{
    class DataProvider
    {
        //VatHolder vat = new VatHolder();
        //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ZATP"].ConnectionString);
        private string connString;
        private SqlConnection conn;// = new SqlConnection("Data Source=ATPSQL\\PRIORITY;Initial Catalog=atpn;Persist Security Info=True;User ID=sa;Password=March@tp");
        private SqlCommand comm;// = new SqlCommand();

        public DataProvider(string connName)
        {
            switch (connName)
            {
                case "atp":
                    connString = ConfigurationManager.ConnectionStrings["atp"].ConnectionString;
                    break;
                case "ca":
                    connString = ConfigurationManager.ConnectionStrings["ca"].ConnectionString;
                    break;
                case "purus":
                    connString = ConfigurationManager.ConnectionStrings["purus"].ConnectionString;
                    break;
                default:
                    connString = ConfigurationManager.ConnectionStrings["atp"].ConnectionString;
                    break;
            }

        }



        // method to get Tax data for sales
        public VatHolder SalesVAT(int type, string startDate, string endDate)
        {
            VatHolder vat = new VatHolder();
            using (conn = new SqlConnection(connString))
            {
                using (comm = new SqlCommand())
                {
                    try
                    {
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.CommandText = "ZATP_VATRETURN_BASIC";
                        comm.Parameters.AddWithValue("@startdate", startDate);
                        comm.Parameters.AddWithValue("@enddate", endDate);
                        conn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(comm);
                        DataTable data = new DataTable();
                        adapter.Fill(data);
                        vat.VATRSALES = data.Rows[0][0].ToString();
                        vat.VATPURCH = data.Rows[0][1].ToString();
                        vat.SALESTOEU = data.Rows[0][2].ToString();
                        vat.SALESFROMEU = data.Rows[0][3].ToString();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            return vat;
        }
        public List<IntrastatModel> IntrastatReportData(string startDate, string endDate, string flow)
        {
            List<IntrastatModel> data = new List<IntrastatModel>();
            using (conn = new SqlConnection(connString))
            {
                using (comm = new SqlCommand())
                {
                    try
                    {
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.CommandText = "[dbo].[ZATP_INTRASTAT]";
                        comm.Parameters.AddWithValue("@startdate", startDate);
                        comm.Parameters.AddWithValue("@enddate", endDate);
                        comm.Parameters.AddWithValue("@flow", flow);
                        conn.Open();
                        SqlDataReader reader = comm.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                IntrastatModel m = new IntrastatModel();
                                int i;
                                i = reader.GetOrdinal("COUNTRY CODE");
                                if (!reader.IsDBNull(i))
                                    m.CountryCode = reader.GetString(i);

                                i = reader.GetOrdinal("COMMODITY CODE");
                                if (!reader.IsDBNull(i))
                                    m.CommodityCode = reader.GetString(i);

                                i = reader.GetOrdinal("SHIPPED QTY");
                                if (!reader.IsDBNull(i))
                                    m.ShippedQty = reader.GetInt32(i);

                                i = reader.GetOrdinal("SUP UNIT");
                                if (!reader.IsDBNull(i))
                                    m.SupplyUnit = reader.GetString(i);

                                i = reader.GetOrdinal("QUANTITY");
                                if (!reader.IsDBNull(i))
                                    m.Quantity = reader.GetString(i);

                                i = reader.GetOrdinal("VALUE");
                                if (!reader.IsDBNull(i))
                                    m.Value = (int)reader.GetDouble(i);

                                i = reader.GetOrdinal("WIEGHT");
                                if (!reader.IsDBNull(i))
                                    m.Weight = (int)reader.GetDouble(i);

                                data.Add(m);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        conn.Close();
                        throw;
                    }
                }
            }
            return data;
        }
        public DataTable ViesReportData(string startDate, string endDate)
        {
            DataTable data = new DataTable();
            using (conn = new SqlConnection(connString))
            {
                using (comm = new SqlCommand())
                {
                    try
                    {
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.StoredProcedure;
                        comm.CommandText = "ZATP_VIES";
                        comm.Parameters.AddWithValue("@startdate", startDate);
                        comm.Parameters.AddWithValue("@enddate", endDate);
                        conn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(comm);
                        adapter.Fill(data);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            return data;
        }

        public List<VatTransaction> getTransactions(VAT_TRANSACTIONS_TYPE type, string startdate, string enddate)
        {
            List<VatTransaction> transactions = new List<VatTransaction>();
            using (conn = new SqlConnection(connString))
            {
                using (comm = new SqlCommand())
                {
                    try
                    {
                        comm.Connection = conn;
                        comm.CommandType = System.Data.CommandType.Text;
                        comm.CommandText = String.Format(SQLCommandProvider.GetInvoiceCommand(type), startdate, enddate);
                        conn.Open();
                        SqlDataReader reader = comm.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                VatTransaction t = new VatTransaction();
                                int i;
                                i = reader.GetOrdinal("CUSTNAME");
                                if (!reader.IsDBNull(i))
                                    t.CustomerNumber = reader.GetString(i);
                                else
                                    continue;
                                i = reader.GetOrdinal("CUSTDES");
                                if (!reader.IsDBNull(i))
                                    t.CustomerName = reader.GetString(i);
                                i = reader.GetOrdinal("IVNUM");
                                if (!reader.IsDBNull(i))
                                    t.InvoiceNumber = reader.GetString(i);
                                i = reader.GetOrdinal("IVDATE");
                                if (!reader.IsDBNull(i))
                                    t.InvoiceDate = reader.GetDateTime(i).ToShortDateString();
                                i = reader.GetOrdinal("QPRICE");
                                if (!reader.IsDBNull(i))
                                    t.InvoiceValue = reader.GetDouble(i);
                                i = reader.GetOrdinal("VAT");
                                if (!reader.IsDBNull(i))
                                    t.InvoiceVat = reader.GetDouble(i);
                                i = reader.GetOrdinal("TOTPRICE");
                                if (!reader.IsDBNull(i))
                                    t.InvoiceTotal = reader.GetDouble(i);
                                transactions.Add(t);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            return transactions;
        }

        // metod to recieve invoices for selected data
    }
}
