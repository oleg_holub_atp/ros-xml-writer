﻿namespace ROS_XML_WRITER
{
    partial class VatTransactions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExport = new System.Windows.Forms.Button();
            this.rbSalesFromEu = new System.Windows.Forms.RadioButton();
            this.rbSalesToEu = new System.Windows.Forms.RadioButton();
            this.rbVatOnPurchase = new System.Windows.Forms.RadioButton();
            this.rbVatOnSales = new System.Windows.Forms.RadioButton();
            this.gvTransactions = new Telerik.WinControls.UI.RadGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvTransactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTransactions.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.rbSalesFromEu);
            this.panel1.Controls.Add(this.rbSalesToEu);
            this.panel1.Controls.Add(this.rbVatOnPurchase);
            this.panel1.Controls.Add(this.rbVatOnSales);
            this.panel1.Location = new System.Drawing.Point(13, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1216, 50);
            this.panel1.TabIndex = 1;
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(1071, 12);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(127, 23);
            this.btnExport.TabIndex = 4;
            this.btnExport.Text = "Export to Excel";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // rbSalesFromEu
            // 
            this.rbSalesFromEu.AutoSize = true;
            this.rbSalesFromEu.Location = new System.Drawing.Point(665, 15);
            this.rbSalesFromEu.Name = "rbSalesFromEu";
            this.rbSalesFromEu.Size = new System.Drawing.Size(159, 17);
            this.rbSalesFromEu.TabIndex = 3;
            this.rbSalesFromEu.Text = "Sales From EU Transactions";
            this.rbSalesFromEu.UseVisualStyleBackColor = true;
            this.rbSalesFromEu.CheckedChanged += new System.EventHandler(this.RadioButton_ChackedChanged);
            // 
            // rbSalesToEu
            // 
            this.rbSalesToEu.AutoSize = true;
            this.rbSalesToEu.Location = new System.Drawing.Point(458, 15);
            this.rbSalesToEu.Name = "rbSalesToEu";
            this.rbSalesToEu.Size = new System.Drawing.Size(149, 17);
            this.rbSalesToEu.TabIndex = 2;
            this.rbSalesToEu.Text = "Sales To EU Transactions";
            this.rbSalesToEu.UseVisualStyleBackColor = true;
            this.rbSalesToEu.CheckedChanged += new System.EventHandler(this.RadioButton_ChackedChanged);
            // 
            // rbVatOnPurchase
            // 
            this.rbVatOnPurchase.AutoSize = true;
            this.rbVatOnPurchase.Location = new System.Drawing.Point(240, 15);
            this.rbVatOnPurchase.Name = "rbVatOnPurchase";
            this.rbVatOnPurchase.Size = new System.Drawing.Size(170, 17);
            this.rbVatOnPurchase.TabIndex = 1;
            this.rbVatOnPurchase.Text = "Vat On Purchase Transactions";
            this.rbVatOnPurchase.UseVisualStyleBackColor = true;
            this.rbVatOnPurchase.CheckedChanged += new System.EventHandler(this.RadioButton_ChackedChanged);
            // 
            // rbVatOnSales
            // 
            this.rbVatOnSales.AutoSize = true;
            this.rbVatOnSales.Checked = true;
            this.rbVatOnSales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbVatOnSales.Location = new System.Drawing.Point(26, 15);
            this.rbVatOnSales.Name = "rbVatOnSales";
            this.rbVatOnSales.Size = new System.Drawing.Size(176, 17);
            this.rbVatOnSales.TabIndex = 0;
            this.rbVatOnSales.TabStop = true;
            this.rbVatOnSales.Text = "Vat On Sales Transactions";
            this.rbVatOnSales.UseVisualStyleBackColor = true;
            this.rbVatOnSales.CheckedChanged += new System.EventHandler(this.RadioButton_ChackedChanged);
            // 
            // gvTransactions
            // 
            this.gvTransactions.Location = new System.Drawing.Point(13, 81);
            // 
            // gvTransactions
            // 
            this.gvTransactions.MasterTemplate.AllowAddNewRow = false;
            this.gvTransactions.Name = "gvTransactions";
            this.gvTransactions.ReadOnly = true;
            this.gvTransactions.Size = new System.Drawing.Size(1216, 592);
            this.gvTransactions.TabIndex = 2;
            // 
            // VatTransactions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1241, 685);
            this.Controls.Add(this.gvTransactions);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VatTransactions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Transactions Details";
            this.Load += new System.EventHandler(this.VatTransactions_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvTransactions.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTransactions)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbSalesFromEu;
        private System.Windows.Forms.RadioButton rbSalesToEu;
        private System.Windows.Forms.RadioButton rbVatOnPurchase;
        private System.Windows.Forms.RadioButton rbVatOnSales;
        private Telerik.WinControls.UI.RadGridView gvTransactions;
        private System.Windows.Forms.Button btnExport;
    }
}