﻿namespace ROS_XML_Writer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.RadButton rbVIES_Go;
            Telerik.WinControls.UI.RadListDataItem radListDataItem1 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem2 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem3 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem4 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem5 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem6 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem7 = new Telerik.WinControls.UI.RadListDataItem();
            Telerik.WinControls.UI.RadListDataItem radListDataItem8 = new Telerik.WinControls.UI.RadListDataItem();
            this.tabSelection = new Telerik.WinControls.UI.RadPageView();
            this.tab1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.rbVAT_Create = new Telerik.WinControls.UI.RadButton();
            this.rpSales = new Telerik.WinControls.UI.RadPanel();
            this.btnViewVatTransactions = new System.Windows.Forms.Button();
            this.radLabel52 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel51 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel50 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel49 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel48 = new Telerik.WinControls.UI.RadLabel();
            this.rtbVAT_FromEU = new Telerik.WinControls.UI.RadTextBox();
            this.rtbVAT_ToEU = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel47 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel46 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel45 = new Telerik.WinControls.UI.RadLabel();
            this.rlDifference = new Telerik.WinControls.UI.RadLabel();
            this.radLabel43 = new Telerik.WinControls.UI.RadLabel();
            this.rtbVAT_Purchs = new Telerik.WinControls.UI.RadTextBox();
            this.rtbVAT_Sales = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel42 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel41 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel40 = new Telerik.WinControls.UI.RadLabel();
            this.lblVat3SortCode = new Telerik.WinControls.UI.RadLabel();
            this.lblVat3BankAccount = new Telerik.WinControls.UI.RadLabel();
            this.radLabel37 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel36 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel35 = new Telerik.WinControls.UI.RadLabel();
            this.lblVat3Account = new Telerik.WinControls.UI.RadLabel();
            this.lblVat3CompanyName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel32 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel31 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel30 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel22 = new Telerik.WinControls.UI.RadLabel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.rcbVAT_Type = new Telerik.WinControls.UI.RadDropDownList();
            this.rcbTAX_Freq = new Telerik.WinControls.UI.RadDropDownList();
            this.radDateTimePicker2 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radDateTimePicker1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.tab2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.rbINT_send = new Telerik.WinControls.UI.RadButton();
            this.rpINT_Results = new Telerik.WinControls.UI.RadPanel();
            this.rgvINT_Results = new Telerik.WinControls.UI.RadGridView();
            this.radPanel3 = new Telerik.WinControls.UI.RadPanel();
            this.lblFlow = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbDispatch = new System.Windows.Forms.RadioButton();
            this.rbArrival = new System.Windows.Forms.RadioButton();
            this.rbINT_Go = new Telerik.WinControls.UI.RadButton();
            this.rdpINT_To = new Telerik.WinControls.UI.RadDateTimePicker();
            this.rdpINT_From = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel54 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel53 = new Telerik.WinControls.UI.RadLabel();
            this.tab3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.rbVIES_Create = new Telerik.WinControls.UI.RadButton();
            this.rpVIES_Results = new Telerik.WinControls.UI.RadPanel();
            this.rgvVIES_Results = new Telerik.WinControls.UI.RadGridView();
            this.radPanel4 = new Telerik.WinControls.UI.RadPanel();
            this.rcbVIES_type = new Telerik.WinControls.UI.RadDropDownList();
            this.rdpVIES_To = new Telerik.WinControls.UI.RadDateTimePicker();
            this.rdpVIES_From = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel57 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel56 = new Telerik.WinControls.UI.RadLabel();
            this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
            this.radMenuItem1 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem2 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
            this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
            rbVIES_Go = new Telerik.WinControls.UI.RadButton();
            ((System.ComponentModel.ISupportInitialize)(rbVIES_Go)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSelection)).BeginInit();
            this.tabSelection.SuspendLayout();
            this.tab1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbVAT_Create)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpSales)).BeginInit();
            this.rpSales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_FromEU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_ToEU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlDifference)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_Purchs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_Sales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3SortCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3BankAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3Account)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3CompanyName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVAT_Type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTAX_Freq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            this.tab2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbINT_send)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpINT_Results)).BeginInit();
            this.rpINT_Results.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgvINT_Results)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvINT_Results.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).BeginInit();
            this.radPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbINT_Go)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpINT_To)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpINT_From)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel53)).BeginInit();
            this.tab3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbVIES_Create)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpVIES_Results)).BeginInit();
            this.rpVIES_Results.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rgvVIES_Results)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvVIES_Results.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).BeginInit();
            this.radPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVIES_type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpVIES_To)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpVIES_From)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // rbVIES_Go
            // 
            rbVIES_Go.Location = new System.Drawing.Point(307, 35);
            rbVIES_Go.Name = "rbVIES_Go";
            rbVIES_Go.Size = new System.Drawing.Size(223, 24);
            rbVIES_Go.TabIndex = 5;
            rbVIES_Go.Text = "GO -->";
            rbVIES_Go.Click += new System.EventHandler(this.rbVIES_Go_Click);
            // 
            // tabSelection
            // 
            this.tabSelection.Controls.Add(this.tab1);
            this.tabSelection.Controls.Add(this.tab2);
            this.tabSelection.Controls.Add(this.tab3);
            this.tabSelection.Location = new System.Drawing.Point(-4, 26);
            this.tabSelection.Name = "tabSelection";
            this.tabSelection.SelectedPage = this.tab1;
            this.tabSelection.Size = new System.Drawing.Size(640, 763);
            this.tabSelection.TabIndex = 0;
            this.tabSelection.ViewMode = Telerik.WinControls.UI.PageViewMode.Outlook;
            // 
            // tab1
            // 
            this.tab1.Controls.Add(this.rbVAT_Create);
            this.tab1.Controls.Add(this.rpSales);
            this.tab1.Controls.Add(this.radPanel1);
            this.tab1.Location = new System.Drawing.Point(5, 31);
            this.tab1.Name = "tab1";
            this.tab1.Size = new System.Drawing.Size(630, 590);
            this.tab1.Text = "VAT RETURN";
            // 
            // rbVAT_Create
            // 
            this.rbVAT_Create.Location = new System.Drawing.Point(208, 560);
            this.rbVAT_Create.Name = "rbVAT_Create";
            this.rbVAT_Create.Size = new System.Drawing.Size(212, 24);
            this.rbVAT_Create.TabIndex = 3;
            this.rbVAT_Create.Text = "CREATE XML FILE";
            this.rbVAT_Create.Click += new System.EventHandler(this.rbVAT_Create_Click);
            // 
            // rpSales
            // 
            this.rpSales.Controls.Add(this.btnViewVatTransactions);
            this.rpSales.Controls.Add(this.radLabel52);
            this.rpSales.Controls.Add(this.radLabel51);
            this.rpSales.Controls.Add(this.radLabel50);
            this.rpSales.Controls.Add(this.radLabel49);
            this.rpSales.Controls.Add(this.radLabel48);
            this.rpSales.Controls.Add(this.rtbVAT_FromEU);
            this.rpSales.Controls.Add(this.rtbVAT_ToEU);
            this.rpSales.Controls.Add(this.radLabel47);
            this.rpSales.Controls.Add(this.radLabel46);
            this.rpSales.Controls.Add(this.radLabel45);
            this.rpSales.Controls.Add(this.rlDifference);
            this.rpSales.Controls.Add(this.radLabel43);
            this.rpSales.Controls.Add(this.rtbVAT_Purchs);
            this.rpSales.Controls.Add(this.rtbVAT_Sales);
            this.rpSales.Controls.Add(this.radLabel42);
            this.rpSales.Controls.Add(this.radLabel41);
            this.rpSales.Controls.Add(this.radLabel40);
            this.rpSales.Controls.Add(this.lblVat3SortCode);
            this.rpSales.Controls.Add(this.lblVat3BankAccount);
            this.rpSales.Controls.Add(this.radLabel37);
            this.rpSales.Controls.Add(this.radLabel36);
            this.rpSales.Controls.Add(this.radLabel35);
            this.rpSales.Controls.Add(this.lblVat3Account);
            this.rpSales.Controls.Add(this.lblVat3CompanyName);
            this.rpSales.Controls.Add(this.radLabel32);
            this.rpSales.Controls.Add(this.radLabel31);
            this.rpSales.Controls.Add(this.radLabel30);
            this.rpSales.Controls.Add(this.radLabel22);
            this.rpSales.Location = new System.Drawing.Point(43, 108);
            this.rpSales.Name = "rpSales";
            this.rpSales.Size = new System.Drawing.Size(548, 446);
            this.rpSales.TabIndex = 2;
            this.rpSales.Visible = false;
            // 
            // btnViewVatTransactions
            // 
            this.btnViewVatTransactions.Location = new System.Drawing.Point(398, 12);
            this.btnViewVatTransactions.Name = "btnViewVatTransactions";
            this.btnViewVatTransactions.Size = new System.Drawing.Size(122, 32);
            this.btnViewVatTransactions.TabIndex = 25;
            this.btnViewVatTransactions.Text = "View Transactions";
            this.btnViewVatTransactions.UseVisualStyleBackColor = true;
            this.btnViewVatTransactions.Click += new System.EventHandler(this.btnViewVatTransactions_Click);
            // 
            // radLabel52
            // 
            this.radLabel52.Location = new System.Drawing.Point(182, 416);
            this.radLabel52.Name = "radLabel52";
            this.radLabel52.Size = new System.Drawing.Size(12, 18);
            this.radLabel52.TabIndex = 24;
            this.radLabel52.Text = "€";
            // 
            // radLabel51
            // 
            this.radLabel51.Location = new System.Drawing.Point(182, 382);
            this.radLabel51.Name = "radLabel51";
            this.radLabel51.Size = new System.Drawing.Size(12, 18);
            this.radLabel51.TabIndex = 24;
            this.radLabel51.Text = "€";
            // 
            // radLabel50
            // 
            this.radLabel50.Location = new System.Drawing.Point(182, 319);
            this.radLabel50.Name = "radLabel50";
            this.radLabel50.Size = new System.Drawing.Size(12, 18);
            this.radLabel50.TabIndex = 24;
            this.radLabel50.Text = "€";
            // 
            // radLabel49
            // 
            this.radLabel49.Location = new System.Drawing.Point(182, 290);
            this.radLabel49.Name = "radLabel49";
            this.radLabel49.Size = new System.Drawing.Size(12, 18);
            this.radLabel49.TabIndex = 24;
            this.radLabel49.Text = "€";
            // 
            // radLabel48
            // 
            this.radLabel48.Location = new System.Drawing.Point(182, 250);
            this.radLabel48.Name = "radLabel48";
            this.radLabel48.Size = new System.Drawing.Size(12, 18);
            this.radLabel48.TabIndex = 23;
            this.radLabel48.Text = "€";
            // 
            // rtbVAT_FromEU
            // 
            this.rtbVAT_FromEU.Location = new System.Drawing.Point(200, 414);
            this.rtbVAT_FromEU.Name = "rtbVAT_FromEU";
            this.rtbVAT_FromEU.Size = new System.Drawing.Size(100, 20);
            this.rtbVAT_FromEU.TabIndex = 22;
            this.rtbVAT_FromEU.TabStop = false;
            // 
            // rtbVAT_ToEU
            // 
            this.rtbVAT_ToEU.Location = new System.Drawing.Point(200, 383);
            this.rtbVAT_ToEU.Name = "rtbVAT_ToEU";
            this.rtbVAT_ToEU.Size = new System.Drawing.Size(100, 20);
            this.rtbVAT_ToEU.TabIndex = 21;
            this.rtbVAT_ToEU.TabStop = false;
            // 
            // radLabel47
            // 
            this.radLabel47.Location = new System.Drawing.Point(34, 417);
            this.radLabel47.Name = "radLabel47";
            this.radLabel47.Size = new System.Drawing.Size(104, 18);
            this.radLabel47.TabIndex = 20;
            this.radLabel47.Text = "Purchases From EU:";
            // 
            // radLabel46
            // 
            this.radLabel46.Location = new System.Drawing.Point(34, 382);
            this.radLabel46.Name = "radLabel46";
            this.radLabel46.Size = new System.Drawing.Size(66, 18);
            this.radLabel46.TabIndex = 19;
            this.radLabel46.Text = "Sales To EU:";
            // 
            // radLabel45
            // 
            this.radLabel45.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.radLabel45.Location = new System.Drawing.Point(200, 353);
            this.radLabel45.Name = "radLabel45";
            this.radLabel45.Size = new System.Drawing.Size(245, 17);
            this.radLabel45.TabIndex = 18;
            this.radLabel45.Text = "TRAIDING WITH OTHER EU COUNTRIES";
            // 
            // rlDifference
            // 
            this.rlDifference.Location = new System.Drawing.Point(200, 319);
            this.rlDifference.Name = "rlDifference";
            this.rlDifference.Size = new System.Drawing.Size(12, 18);
            this.rlDifference.TabIndex = 17;
            this.rlDifference.Text = "0";
            // 
            // radLabel43
            // 
            this.radLabel43.Location = new System.Drawing.Point(34, 325);
            this.radLabel43.Name = "radLabel43";
            this.radLabel43.Size = new System.Drawing.Size(84, 18);
            this.radLabel43.TabIndex = 16;
            this.radLabel43.Text = "NET Repayable:";
            // 
            // rtbVAT_Purchs
            // 
            this.rtbVAT_Purchs.Location = new System.Drawing.Point(200, 288);
            this.rtbVAT_Purchs.Name = "rtbVAT_Purchs";
            this.rtbVAT_Purchs.Size = new System.Drawing.Size(100, 20);
            this.rtbVAT_Purchs.TabIndex = 15;
            this.rtbVAT_Purchs.TabStop = false;
            // 
            // rtbVAT_Sales
            // 
            this.rtbVAT_Sales.Location = new System.Drawing.Point(200, 247);
            this.rtbVAT_Sales.Name = "rtbVAT_Sales";
            this.rtbVAT_Sales.Size = new System.Drawing.Size(100, 20);
            this.rtbVAT_Sales.TabIndex = 14;
            this.rtbVAT_Sales.TabStop = false;
            // 
            // radLabel42
            // 
            this.radLabel42.Location = new System.Drawing.Point(34, 290);
            this.radLabel42.Name = "radLabel42";
            this.radLabel42.Size = new System.Drawing.Size(98, 18);
            this.radLabel42.TabIndex = 13;
            this.radLabel42.Text = "VAT on Purchases:";
            // 
            // radLabel41
            // 
            this.radLabel41.Location = new System.Drawing.Point(34, 250);
            this.radLabel41.Name = "radLabel41";
            this.radLabel41.Size = new System.Drawing.Size(70, 18);
            this.radLabel41.TabIndex = 12;
            this.radLabel41.Text = "Vat on Sales:";
            // 
            // radLabel40
            // 
            this.radLabel40.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.radLabel40.Location = new System.Drawing.Point(200, 221);
            this.radLabel40.Name = "radLabel40";
            this.radLabel40.Size = new System.Drawing.Size(88, 17);
            this.radLabel40.TabIndex = 11;
            this.radLabel40.Text = "VAT DETAILS";
            // 
            // lblVat3SortCode
            // 
            this.lblVat3SortCode.Location = new System.Drawing.Point(200, 185);
            this.lblVat3SortCode.Name = "lblVat3SortCode";
            this.lblVat3SortCode.Size = new System.Drawing.Size(52, 18);
            this.lblVat3SortCode.TabIndex = 10;
            this.lblVat3SortCode.Text = "90-43-09";
            // 
            // lblVat3BankAccount
            // 
            this.lblVat3BankAccount.Location = new System.Drawing.Point(200, 161);
            this.lblVat3BankAccount.Name = "lblVat3BankAccount";
            this.lblVat3BankAccount.Size = new System.Drawing.Size(55, 18);
            this.lblVat3BankAccount.TabIndex = 9;
            this.lblVat3BankAccount.Text = "17413418";
            // 
            // radLabel37
            // 
            this.radLabel37.Location = new System.Drawing.Point(34, 191);
            this.radLabel37.Name = "radLabel37";
            this.radLabel37.Size = new System.Drawing.Size(58, 18);
            this.radLabel37.TabIndex = 8;
            this.radLabel37.Text = "Sort Code:";
            // 
            // radLabel36
            // 
            this.radLabel36.Location = new System.Drawing.Point(34, 167);
            this.radLabel36.Name = "radLabel36";
            this.radLabel36.Size = new System.Drawing.Size(94, 18);
            this.radLabel36.TabIndex = 7;
            this.radLabel36.Text = "Account Number:";
            // 
            // radLabel35
            // 
            this.radLabel35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.radLabel35.Location = new System.Drawing.Point(200, 138);
            this.radLabel35.Name = "radLabel35";
            this.radLabel35.Size = new System.Drawing.Size(194, 17);
            this.radLabel35.TabIndex = 6;
            this.radLabel35.Text = "ROS BANK ACCOUNT DETAILS";
            // 
            // lblVat3Account
            // 
            this.lblVat3Account.Location = new System.Drawing.Point(200, 97);
            this.lblVat3Account.Name = "lblVat3Account";
            this.lblVat3Account.Size = new System.Drawing.Size(55, 18);
            this.lblVat3Account.TabIndex = 5;
            this.lblVat3Account.Text = "4841327K";
            // 
            // lblVat3CompanyName
            // 
            this.lblVat3CompanyName.Location = new System.Drawing.Point(200, 66);
            this.lblVat3CompanyName.Name = "lblVat3CompanyName";
            this.lblVat3CompanyName.Size = new System.Drawing.Size(177, 18);
            this.lblVat3CompanyName.TabIndex = 4;
            this.lblVat3CompanyName.Text = "TOWERWOOD INVESTMENTS LTD";
            // 
            // radLabel32
            // 
            this.radLabel32.Location = new System.Drawing.Point(34, 97);
            this.radLabel32.Name = "radLabel32";
            this.radLabel32.Size = new System.Drawing.Size(113, 18);
            this.radLabel32.TabIndex = 3;
            this.radLabel32.Text = "Registration Number:";
            // 
            // radLabel31
            // 
            this.radLabel31.Location = new System.Drawing.Point(34, 72);
            this.radLabel31.Name = "radLabel31";
            this.radLabel31.Size = new System.Drawing.Size(56, 18);
            this.radLabel31.TabIndex = 2;
            this.radLabel31.Text = "Company:";
            // 
            // radLabel30
            // 
            this.radLabel30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.radLabel30.Location = new System.Drawing.Point(200, 43);
            this.radLabel30.Name = "radLabel30";
            this.radLabel30.Size = new System.Drawing.Size(126, 17);
            this.radLabel30.TabIndex = 1;
            this.radLabel30.Text = "COMPANY DETAILS";
            // 
            // radLabel22
            // 
            this.radLabel22.Font = new System.Drawing.Font("Arial", 14F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))));
            this.radLabel22.Location = new System.Drawing.Point(201, 12);
            this.radLabel22.Name = "radLabel22";
            this.radLabel22.Size = new System.Drawing.Size(125, 25);
            this.radLabel22.TabIndex = 0;
            this.radLabel22.Text = "VAT3 Return";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radButton1);
            this.radPanel1.Controls.Add(this.rcbVAT_Type);
            this.radPanel1.Controls.Add(this.rcbTAX_Freq);
            this.radPanel1.Controls.Add(this.radDateTimePicker2);
            this.radPanel1.Controls.Add(this.radDateTimePicker1);
            this.radPanel1.Controls.Add(this.radLabel2);
            this.radPanel1.Controls.Add(this.radLabel1);
            this.radPanel1.Location = new System.Drawing.Point(43, 3);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(548, 99);
            this.radPanel1.TabIndex = 1;
            // 
            // radButton1
            // 
            this.radButton1.Location = new System.Drawing.Point(181, 64);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(196, 24);
            this.radButton1.TabIndex = 6;
            this.radButton1.Text = "GO-->";
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click);
            // 
            // rcbVAT_Type
            // 
            this.rcbVAT_Type.DropDownAnimationEnabled = true;
            radListDataItem1.Text = "Original VAT3 Return";
            radListDataItem1.TextWrap = true;
            radListDataItem2.Text = "Supplementary VAT3 Return";
            radListDataItem2.TextWrap = true;
            this.rcbVAT_Type.Items.Add(radListDataItem1);
            this.rcbVAT_Type.Items.Add(radListDataItem2);
            this.rcbVAT_Type.Location = new System.Drawing.Point(324, 38);
            this.rcbVAT_Type.Name = "rcbVAT_Type";
            this.rcbVAT_Type.ShowImageInEditorArea = true;
            this.rcbVAT_Type.Size = new System.Drawing.Size(196, 20);
            this.rcbVAT_Type.TabIndex = 5;
            // 
            // rcbTAX_Freq
            // 
            this.rcbTAX_Freq.DropDownAnimationEnabled = true;
            radListDataItem3.Text = "Bi-Montly/Repayment Only";
            radListDataItem3.TextWrap = true;
            radListDataItem4.Text = "Bi-Annual";
            radListDataItem4.TextWrap = true;
            this.rcbTAX_Freq.Items.Add(radListDataItem3);
            this.rcbTAX_Freq.Items.Add(radListDataItem4);
            this.rcbTAX_Freq.Location = new System.Drawing.Point(324, 12);
            this.rcbTAX_Freq.Name = "rcbTAX_Freq";
            this.rcbTAX_Freq.ShowImageInEditorArea = true;
            this.rcbTAX_Freq.Size = new System.Drawing.Size(196, 20);
            this.rcbTAX_Freq.TabIndex = 4;
            // 
            // radDateTimePicker2
            // 
            this.radDateTimePicker2.Culture = new System.Globalization.CultureInfo("en-GB");
            this.radDateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.radDateTimePicker2.Location = new System.Drawing.Point(91, 38);
            this.radDateTimePicker2.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.radDateTimePicker2.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker2.Name = "radDateTimePicker2";
            this.radDateTimePicker2.NullableValue = new System.DateTime(2010, 9, 29, 17, 6, 55, 765);
            this.radDateTimePicker2.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker2.Size = new System.Drawing.Size(184, 20);
            this.radDateTimePicker2.TabIndex = 3;
            this.radDateTimePicker2.TabStop = false;
            this.radDateTimePicker2.Text = "radDateTimePicker2";
            this.radDateTimePicker2.Value = new System.DateTime(2010, 9, 29, 17, 6, 55, 765);
            this.radDateTimePicker2.ValueChanged += new System.EventHandler(this.radDateTimePicker2_ValueChanged);
            // 
            // radDateTimePicker1
            // 
            this.radDateTimePicker1.Culture = new System.Globalization.CultureInfo("en-GB");
            this.radDateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.radDateTimePicker1.Location = new System.Drawing.Point(91, 12);
            this.radDateTimePicker1.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.radDateTimePicker1.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker1.Name = "radDateTimePicker1";
            this.radDateTimePicker1.NullableValue = new System.DateTime(2010, 9, 29, 17, 5, 38, 984);
            this.radDateTimePicker1.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.radDateTimePicker1.Size = new System.Drawing.Size(184, 20);
            this.radDateTimePicker1.TabIndex = 2;
            this.radDateTimePicker1.TabStop = false;
            this.radDateTimePicker1.Value = new System.DateTime(2010, 9, 29, 17, 5, 38, 984);
            this.radDateTimePicker1.ValueChanged += new System.EventHandler(this.radDateTimePicker1_ValueChanged);
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(26, 38);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(19, 18);
            this.radLabel2.TabIndex = 1;
            this.radLabel2.Text = "To";
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(26, 14);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(32, 18);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "From";
            // 
            // tab2
            // 
            this.tab2.Controls.Add(this.rbINT_send);
            this.tab2.Controls.Add(this.rpINT_Results);
            this.tab2.Controls.Add(this.radPanel3);
            this.tab2.Location = new System.Drawing.Point(5, 31);
            this.tab2.Name = "tab2";
            this.tab2.Size = new System.Drawing.Size(630, 590);
            this.tab2.Text = "INTRASTAT";
            // 
            // rbINT_send
            // 
            this.rbINT_send.Location = new System.Drawing.Point(208, 448);
            this.rbINT_send.Name = "rbINT_send";
            this.rbINT_send.Size = new System.Drawing.Size(215, 24);
            this.rbINT_send.TabIndex = 3;
            this.rbINT_send.Text = "CREATE XML FILE";
            this.rbINT_send.Visible = false;
            this.rbINT_send.Click += new System.EventHandler(this.rbINT_send_Click);
            // 
            // rpINT_Results
            // 
            this.rpINT_Results.Controls.Add(this.rgvINT_Results);
            this.rpINT_Results.Location = new System.Drawing.Point(44, 112);
            this.rpINT_Results.Name = "rpINT_Results";
            this.rpINT_Results.Size = new System.Drawing.Size(548, 330);
            this.rpINT_Results.TabIndex = 2;
            this.rpINT_Results.Visible = false;
            // 
            // rgvINT_Results
            // 
            this.rgvINT_Results.Location = new System.Drawing.Point(15, 13);
            // 
            // 
            // 
            this.rgvINT_Results.MasterTemplate.AllowColumnReorder = false;
            this.rgvINT_Results.MasterTemplate.AllowDragToGroup = false;
            this.rgvINT_Results.MasterTemplate.AllowRowResize = false;
            this.rgvINT_Results.Name = "rgvINT_Results";
            this.rgvINT_Results.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.rgvINT_Results.ReadOnly = true;
            // 
            // 
            // 
            this.rgvINT_Results.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.rgvINT_Results.Size = new System.Drawing.Size(518, 302);
            this.rgvINT_Results.TabIndex = 0;
            // 
            // radPanel3
            // 
            this.radPanel3.Controls.Add(this.lblFlow);
            this.radPanel3.Controls.Add(this.panel1);
            this.radPanel3.Controls.Add(this.rbINT_Go);
            this.radPanel3.Controls.Add(this.rdpINT_To);
            this.radPanel3.Controls.Add(this.rdpINT_From);
            this.radPanel3.Controls.Add(this.radLabel54);
            this.radPanel3.Controls.Add(this.radLabel53);
            this.radPanel3.Location = new System.Drawing.Point(43, 3);
            this.radPanel3.Name = "radPanel3";
            this.radPanel3.Size = new System.Drawing.Size(548, 87);
            this.radPanel3.TabIndex = 0;
            // 
            // lblFlow
            // 
            this.lblFlow.AutoSize = true;
            this.lblFlow.Location = new System.Drawing.Point(17, 51);
            this.lblFlow.Name = "lblFlow";
            this.lblFlow.Size = new System.Drawing.Size(32, 13);
            this.lblFlow.TabIndex = 6;
            this.lblFlow.Text = "Flow";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbDispatch);
            this.panel1.Controls.Add(this.rbArrival);
            this.panel1.Location = new System.Drawing.Point(55, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(344, 25);
            this.panel1.TabIndex = 5;
            // 
            // rbDispatch
            // 
            this.rbDispatch.AutoSize = true;
            this.rbDispatch.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.rbDispatch.Location = new System.Drawing.Point(194, 4);
            this.rbDispatch.Name = "rbDispatch";
            this.rbDispatch.Size = new System.Drawing.Size(112, 17);
            this.rbDispatch.TabIndex = 1;
            this.rbDispatch.Text = "Dispatch(Export)";
            this.rbDispatch.UseVisualStyleBackColor = true;
            this.rbDispatch.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // rbArrival
            // 
            this.rbArrival.AutoSize = true;
            this.rbArrival.Checked = true;
            this.rbArrival.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.rbArrival.ForeColor = System.Drawing.Color.Red;
            this.rbArrival.Location = new System.Drawing.Point(15, 5);
            this.rbArrival.Name = "rbArrival";
            this.rbArrival.Size = new System.Drawing.Size(102, 17);
            this.rbArrival.TabIndex = 0;
            this.rbArrival.TabStop = true;
            this.rbArrival.Text = "Arrival(Import)";
            this.rbArrival.UseVisualStyleBackColor = true;
            this.rbArrival.CheckedChanged += new System.EventHandler(this.RadioButton_CheckedChanged);
            // 
            // rbINT_Go
            // 
            this.rbINT_Go.Location = new System.Drawing.Point(405, 8);
            this.rbINT_Go.Name = "rbINT_Go";
            this.rbINT_Go.Size = new System.Drawing.Size(102, 20);
            this.rbINT_Go.TabIndex = 4;
            this.rbINT_Go.Text = "GO -->";
            this.rbINT_Go.Click += new System.EventHandler(this.rbINT_Go_Click);
            // 
            // rdpINT_To
            // 
            this.rdpINT_To.Culture = new System.Globalization.CultureInfo("en-GB");
            this.rdpINT_To.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.rdpINT_To.Location = new System.Drawing.Point(249, 8);
            this.rdpINT_To.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.rdpINT_To.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpINT_To.Name = "rdpINT_To";
            this.rdpINT_To.NullableValue = new System.DateTime(2010, 9, 30, 10, 29, 55, 125);
            this.rdpINT_To.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpINT_To.Size = new System.Drawing.Size(150, 20);
            this.rdpINT_To.TabIndex = 3;
            this.rdpINT_To.TabStop = false;
            this.rdpINT_To.Text = "radDateTimePicker4";
            this.rdpINT_To.Value = new System.DateTime(2010, 9, 30, 10, 29, 55, 125);
            this.rdpINT_To.ValueChanged += new System.EventHandler(this.rdpINT_To_ValueChanged);
            // 
            // rdpINT_From
            // 
            this.rdpINT_From.Culture = new System.Globalization.CultureInfo("en-GB");
            this.rdpINT_From.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.rdpINT_From.Location = new System.Drawing.Point(55, 8);
            this.rdpINT_From.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.rdpINT_From.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpINT_From.Name = "rdpINT_From";
            this.rdpINT_From.NullableValue = new System.DateTime(2010, 9, 30, 10, 29, 50, 359);
            this.rdpINT_From.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpINT_From.Size = new System.Drawing.Size(150, 20);
            this.rdpINT_From.TabIndex = 2;
            this.rdpINT_From.TabStop = false;
            this.rdpINT_From.Text = "radDateTimePicker3";
            this.rdpINT_From.Value = new System.DateTime(2010, 9, 30, 10, 29, 50, 359);
            this.rdpINT_From.ValueChanged += new System.EventHandler(this.rdpINT_From_ValueChanged);
            // 
            // radLabel54
            // 
            this.radLabel54.Location = new System.Drawing.Point(222, 8);
            this.radLabel54.Name = "radLabel54";
            this.radLabel54.Size = new System.Drawing.Size(21, 18);
            this.radLabel54.TabIndex = 1;
            this.radLabel54.Text = "To:";
            // 
            // radLabel53
            // 
            this.radLabel53.Location = new System.Drawing.Point(15, 8);
            this.radLabel53.Name = "radLabel53";
            this.radLabel53.Size = new System.Drawing.Size(34, 18);
            this.radLabel53.TabIndex = 0;
            this.radLabel53.Text = "From:";
            // 
            // tab3
            // 
            this.tab3.Controls.Add(this.rbVIES_Create);
            this.tab3.Controls.Add(this.rpVIES_Results);
            this.tab3.Controls.Add(this.radPanel4);
            this.tab3.Location = new System.Drawing.Point(5, 31);
            this.tab3.Name = "tab3";
            this.tab3.Size = new System.Drawing.Size(630, 590);
            this.tab3.Text = "VIES";
            // 
            // rbVIES_Create
            // 
            this.rbVIES_Create.Location = new System.Drawing.Point(203, 520);
            this.rbVIES_Create.Name = "rbVIES_Create";
            this.rbVIES_Create.Size = new System.Drawing.Size(241, 24);
            this.rbVIES_Create.TabIndex = 3;
            this.rbVIES_Create.Text = "CREATE XML FILE";
            this.rbVIES_Create.Visible = false;
            this.rbVIES_Create.Click += new System.EventHandler(this.rbVIES_Create_Click);
            // 
            // rpVIES_Results
            // 
            this.rpVIES_Results.Controls.Add(this.rgvVIES_Results);
            this.rpVIES_Results.Location = new System.Drawing.Point(43, 79);
            this.rpVIES_Results.Name = "rpVIES_Results";
            this.rpVIES_Results.Size = new System.Drawing.Size(548, 435);
            this.rpVIES_Results.TabIndex = 2;
            this.rpVIES_Results.Visible = false;
            // 
            // rgvVIES_Results
            // 
            this.rgvVIES_Results.Location = new System.Drawing.Point(18, 16);
            // 
            // 
            // 
            this.rgvVIES_Results.MasterTemplate.AllowColumnReorder = false;
            this.rgvVIES_Results.MasterTemplate.AllowDragToGroup = false;
            this.rgvVIES_Results.Name = "rgvVIES_Results";
            this.rgvVIES_Results.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.rgvVIES_Results.ReadOnly = true;
            // 
            // 
            // 
            this.rgvVIES_Results.RootElement.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.rgvVIES_Results.Size = new System.Drawing.Size(512, 403);
            this.rgvVIES_Results.TabIndex = 0;
            // 
            // radPanel4
            // 
            this.radPanel4.Controls.Add(rbVIES_Go);
            this.radPanel4.Controls.Add(this.rcbVIES_type);
            this.radPanel4.Controls.Add(this.rdpVIES_To);
            this.radPanel4.Controls.Add(this.rdpVIES_From);
            this.radPanel4.Controls.Add(this.radLabel57);
            this.radPanel4.Controls.Add(this.radLabel56);
            this.radPanel4.Location = new System.Drawing.Point(43, 3);
            this.radPanel4.Name = "radPanel4";
            this.radPanel4.Size = new System.Drawing.Size(548, 70);
            this.radPanel4.TabIndex = 0;
            // 
            // rcbVIES_type
            // 
            this.rcbVIES_type.DropDownAnimationEnabled = true;
            radListDataItem5.Text = "Monthly";
            radListDataItem5.TextWrap = true;
            radListDataItem6.Text = "Quarterly";
            radListDataItem6.TextWrap = true;
            radListDataItem7.Text = "Annual-A1";
            radListDataItem7.TextWrap = true;
            radListDataItem8.Text = "Annual-A2";
            radListDataItem8.TextWrap = true;
            this.rcbVIES_type.Items.Add(radListDataItem5);
            this.rcbVIES_type.Items.Add(radListDataItem6);
            this.rcbVIES_type.Items.Add(radListDataItem7);
            this.rcbVIES_type.Items.Add(radListDataItem8);
            this.rcbVIES_type.Location = new System.Drawing.Point(307, 13);
            this.rcbVIES_type.Name = "rcbVIES_type";
            this.rcbVIES_type.ShowImageInEditorArea = true;
            this.rcbVIES_type.Size = new System.Drawing.Size(223, 20);
            this.rcbVIES_type.TabIndex = 4;
            // 
            // rdpVIES_To
            // 
            this.rdpVIES_To.Culture = new System.Globalization.CultureInfo("en-GB");
            this.rdpVIES_To.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.rdpVIES_To.Location = new System.Drawing.Point(114, 39);
            this.rdpVIES_To.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.rdpVIES_To.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpVIES_To.Name = "rdpVIES_To";
            this.rdpVIES_To.NullableValue = new System.DateTime(2010, 9, 30, 10, 33, 23, 843);
            this.rdpVIES_To.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpVIES_To.Size = new System.Drawing.Size(150, 20);
            this.rdpVIES_To.TabIndex = 3;
            this.rdpVIES_To.TabStop = false;
            this.rdpVIES_To.Text = "radDateTimePicker4";
            this.rdpVIES_To.Value = new System.DateTime(2010, 9, 30, 10, 33, 23, 843);
            this.rdpVIES_To.ValueChanged += new System.EventHandler(this.rdpVIES_To_ValueChanged);
            // 
            // rdpVIES_From
            // 
            this.rdpVIES_From.Culture = new System.Globalization.CultureInfo("en-GB");
            this.rdpVIES_From.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.rdpVIES_From.Location = new System.Drawing.Point(114, 13);
            this.rdpVIES_From.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.rdpVIES_From.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpVIES_From.Name = "rdpVIES_From";
            this.rdpVIES_From.NullableValue = new System.DateTime(2010, 9, 30, 10, 33, 21, 328);
            this.rdpVIES_From.NullDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.rdpVIES_From.Size = new System.Drawing.Size(150, 20);
            this.rdpVIES_From.TabIndex = 2;
            this.rdpVIES_From.TabStop = false;
            this.rdpVIES_From.Text = "radDateTimePicker3";
            this.rdpVIES_From.Value = new System.DateTime(2010, 9, 30, 10, 33, 21, 328);
            this.rdpVIES_From.ValueChanged += new System.EventHandler(this.rdpVIES_From_ValueChanged);
            // 
            // radLabel57
            // 
            this.radLabel57.Location = new System.Drawing.Point(42, 41);
            this.radLabel57.Name = "radLabel57";
            this.radLabel57.Size = new System.Drawing.Size(21, 18);
            this.radLabel57.TabIndex = 1;
            this.radLabel57.Text = "To:";
            // 
            // radLabel56
            // 
            this.radLabel56.Location = new System.Drawing.Point(42, 13);
            this.radLabel56.Name = "radLabel56";
            this.radLabel56.Size = new System.Drawing.Size(34, 18);
            this.radLabel56.TabIndex = 0;
            this.radLabel56.Text = "From:";
            // 
            // radMenu1
            // 
            this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem1});
            this.radMenu1.Location = new System.Drawing.Point(0, 0);
            this.radMenu1.Name = "radMenu1";
            this.radMenu1.Size = new System.Drawing.Size(635, 20);
            this.radMenu1.TabIndex = 1;
            this.radMenu1.Text = "radMenu1";
            // 
            // radMenuItem1
            // 
            this.radMenuItem1.AccessibleDescription = "Select Company";
            this.radMenuItem1.AccessibleName = "Select Company";
            this.radMenuItem1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem2,
            this.radMenuItem3,
            this.radMenuItem4});
            this.radMenuItem1.Name = "radMenuItem1";
            this.radMenuItem1.Tag = "0";
            this.radMenuItem1.Text = "Select Company";
            this.radMenuItem1.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            // 
            // radMenuItem2
            // 
            this.radMenuItem2.AccessibleDescription = "radMenuItem2";
            this.radMenuItem2.AccessibleName = "radMenuItem2";
            this.radMenuItem2.Name = "radMenuItem2";
            this.radMenuItem2.Tag = "1";
            this.radMenuItem2.Text = "ATP";
            this.radMenuItem2.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuItem2.Click += new System.EventHandler(this.radMenuItem_Click);
            // 
            // radMenuItem3
            // 
            this.radMenuItem3.AccessibleDescription = "radMenuItem3";
            this.radMenuItem3.AccessibleName = "radMenuItem3";
            this.radMenuItem3.Name = "radMenuItem3";
            this.radMenuItem3.Tag = "2";
            this.radMenuItem3.Text = "Cosmetic Alliance";
            this.radMenuItem3.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuItem3.Click += new System.EventHandler(this.radMenuItem_Click);
            // 
            // radMenuItem4
            // 
            this.radMenuItem4.AccessibleDescription = "radMenuItem4";
            this.radMenuItem4.AccessibleName = "radMenuItem4";
            this.radMenuItem4.Name = "radMenuItem4";
            this.radMenuItem4.Tag = "3";
            this.radMenuItem4.Text = "Purus";
            this.radMenuItem4.Visibility = Telerik.WinControls.ElementVisibility.Visible;
            this.radMenuItem4.Click += new System.EventHandler(this.radMenuItem_Click);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(635, 788);
            this.Controls.Add(this.radMenu1);
            this.Controls.Add(this.tabSelection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ATP ACCOUNT ASSISTANT";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(rbVIES_Go)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabSelection)).EndInit();
            this.tabSelection.ResumeLayout(false);
            this.tab1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbVAT_Create)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpSales)).EndInit();
            this.rpSales.ResumeLayout(false);
            this.rpSales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_FromEU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_ToEU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlDifference)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_Purchs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rtbVAT_Sales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3SortCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3BankAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3Account)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblVat3CompanyName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            this.radPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVAT_Type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTAX_Freq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            this.tab2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbINT_send)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpINT_Results)).EndInit();
            this.rpINT_Results.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgvINT_Results.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvINT_Results)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel3)).EndInit();
            this.radPanel3.ResumeLayout(false);
            this.radPanel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbINT_Go)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpINT_To)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpINT_From)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel53)).EndInit();
            this.tab3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rbVIES_Create)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpVIES_Results)).EndInit();
            this.rpVIES_Results.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rgvVIES_Results.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgvVIES_Results)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel4)).EndInit();
            this.radPanel4.ResumeLayout(false);
            this.radPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVIES_type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpVIES_To)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdpVIES_From)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //private Telerik.WinControls.Themes.VistaTheme vistaTheme1;
        //private Telerik.WinControls.RadThemeManager radThemeManager1;
        //private Telerik.WinControls.UI.RadTabStrip tabSelection;
        //private Telerik.WinControls.UI.TabItem tab1;
        //private Telerik.WinControls.UI.TabItem tab2;
        //private Telerik.WinControls.UI.TabItem tab3;
        //private Telerik.WinControls.UI.RadLabel radLabel1;
        //private Telerik.WinControls.UI.RadPanel radPanel1;
        //private Telerik.WinControls.UI.RadLabel radLabel2;
        //private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        //private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        //private Telerik.WinControls.UI.RadPanel rpSales;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadLabel radLabel11;
        private Telerik.WinControls.UI.RadLabel radLabel9;
        private Telerik.WinControls.UI.RadLabel radLabel12;
        private Telerik.WinControls.UI.RadLabel radLabel7;
        private Telerik.WinControls.UI.RadLabel radLabel10;
        private Telerik.WinControls.UI.RadLabel radLabel6;
        private Telerik.WinControls.UI.RadLabel radLabel8;
        private Telerik.WinControls.UI.RadLabel radLabel5;
        private Telerik.WinControls.UI.RadLabel radLabel23;
        private Telerik.WinControls.UI.RadLabel radLabel21;
        private Telerik.WinControls.UI.RadLabel radLabel19;
        private Telerik.WinControls.UI.RadLabel radLabel15;
        private Telerik.WinControls.UI.RadLabel radLabel17;
        private Telerik.WinControls.UI.RadLabel radLabel14;
        private Telerik.WinControls.UI.RadLabel radLabel13;
        //private Telerik.WinControls.UI.RadLabel rlDifference;
        private Telerik.WinControls.UI.RadLabel radLabel18;
        private Telerik.WinControls.UI.RadLabel radLabel24;
        private Telerik.WinControls.UI.RadLabel radLabel20;
        //private Telerik.WinControls.UI.RadPanel rpINT_Results;
        //private Telerik.WinControls.UI.RadGridView rgvINT_Results;
        private Telerik.WinControls.UI.RadPanel rpINT_Select;
        //private Telerik.WinControls.UI.RadButton rbINT_Go;
        private Telerik.WinControls.UI.RadLabel radLabel25;
        //private Telerik.WinControls.UI.RadDateTimePicker rdpINT_To;
        //private Telerik.WinControls.UI.RadDateTimePicker rdpINT_From;
        //private Telerik.WinControls.UI.RadButton rbINT_send;
        //private Telerik.WinControls.UI.RadComboBox rcbVAT_Type;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem1;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem2;
        private Telerik.WinControls.UI.RadLabel radLabel26;
        //private Telerik.WinControls.UI.RadButton rbVIES_Create;
        //private Telerik.WinControls.UI.RadPanel rpVIES_Results;
        //private Telerik.WinControls.UI.RadDateTimePicker rdpVIES_From;
        private Telerik.WinControls.UI.RadLabel radLabel29;
        private Telerik.WinControls.UI.RadPanel rpVIES_Select;
        //private Telerik.WinControls.UI.RadButton rbVIES_Go;
        private Telerik.WinControls.UI.RadLabel radLabel27;
        private Telerik.WinControls.UI.RadLabel radLabel28;
        //private Telerik.WinControls.UI.RadGridView rgvVIES_Results;
        //private Telerik.WinControls.UI.RadDateTimePicker rdpVIES_To;
        //private Telerik.WinControls.UI.RadComboBox rcbVIES_type;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem4;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem5;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem6;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem7;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem3;
        //private Telerik.WinControls.UI.RadComboBox rcbTAX_Freq;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem8;
        private Telerik.WinControls.UI.RadComboBoxItem radComboBoxItem9;
        //private Telerik.WinControls.UI.RadTextBox rtbVAT_Sales;
        //private Telerik.WinControls.UI.RadTextBox rtbVAT_ToEU;
        //private Telerik.WinControls.UI.RadTextBox rtbVAT_Purchs;
        //private Telerik.WinControls.UI.RadTextBox rtbVAT_FromEU;
        //private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        //private Telerik.WinControls.Themes.VistaTheme vistaTheme2;
        private Telerik.WinControls.Themes.VistaTheme vistaTheme1;
        private Telerik.WinControls.UI.RadPageView tabSelection;
        private Telerik.WinControls.UI.RadPageViewPage tab1;
        private Telerik.WinControls.UI.RadPageViewPage tab2;
        private Telerik.WinControls.UI.RadPageViewPage tab3;
        //private Telerik.WinControls.RadThemeManager radThemeManager2;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker2;
        private Telerik.WinControls.UI.RadButton radButton1;
        private Telerik.WinControls.UI.RadDropDownList rcbVAT_Type;
        private Telerik.WinControls.UI.RadDropDownList rcbTAX_Freq;
        private Telerik.WinControls.UI.RadPanel rpSales;
        private Telerik.WinControls.UI.RadLabel radLabel22;
        private Telerik.WinControls.UI.RadTextBox rtbVAT_FromEU;
        private Telerik.WinControls.UI.RadTextBox rtbVAT_ToEU;
        private Telerik.WinControls.UI.RadLabel radLabel47;
        private Telerik.WinControls.UI.RadLabel radLabel46;
        private Telerik.WinControls.UI.RadLabel radLabel45;
        private Telerik.WinControls.UI.RadLabel rlDifference;
        private Telerik.WinControls.UI.RadLabel radLabel43;
        private Telerik.WinControls.UI.RadTextBox rtbVAT_Purchs;
        private Telerik.WinControls.UI.RadTextBox rtbVAT_Sales;
        private Telerik.WinControls.UI.RadLabel radLabel42;
        private Telerik.WinControls.UI.RadLabel radLabel41;
        private Telerik.WinControls.UI.RadLabel radLabel40;
        private Telerik.WinControls.UI.RadLabel lblVat3SortCode;
        private Telerik.WinControls.UI.RadLabel lblVat3BankAccount;
        private Telerik.WinControls.UI.RadLabel radLabel37;
        private Telerik.WinControls.UI.RadLabel radLabel36;
        private Telerik.WinControls.UI.RadLabel radLabel35;
        private Telerik.WinControls.UI.RadLabel lblVat3Account;
        private Telerik.WinControls.UI.RadLabel lblVat3CompanyName;
        private Telerik.WinControls.UI.RadLabel radLabel32;
        private Telerik.WinControls.UI.RadLabel radLabel31;
        private Telerik.WinControls.UI.RadLabel radLabel30;
        private Telerik.WinControls.UI.RadLabel radLabel52;
        private Telerik.WinControls.UI.RadLabel radLabel51;
        private Telerik.WinControls.UI.RadLabel radLabel50;
        private Telerik.WinControls.UI.RadLabel radLabel49;
        private Telerik.WinControls.UI.RadLabel radLabel48;
        private Telerik.WinControls.UI.RadPanel radPanel3;
        private Telerik.WinControls.UI.RadButton rbINT_Go;
        private Telerik.WinControls.UI.RadDateTimePicker rdpINT_To;
        private Telerik.WinControls.UI.RadDateTimePicker rdpINT_From;
        private Telerik.WinControls.UI.RadLabel radLabel54;
        private Telerik.WinControls.UI.RadLabel radLabel53;
        private Telerik.WinControls.UI.RadPanel radPanel4;
        private Telerik.WinControls.UI.RadDropDownList rcbVIES_type;
        private Telerik.WinControls.UI.RadDateTimePicker rdpVIES_To;
        private Telerik.WinControls.UI.RadDateTimePicker rdpVIES_From;
        private Telerik.WinControls.UI.RadLabel radLabel57;
        private Telerik.WinControls.UI.RadLabel radLabel56;
        private Telerik.WinControls.UI.RadPanel rpINT_Results;
        private Telerik.WinControls.UI.RadGridView rgvINT_Results;
        private Telerik.WinControls.UI.RadButton rbINT_send;
        private Telerik.WinControls.UI.RadButton rbVAT_Create;
        private Telerik.WinControls.UI.RadButton rbVIES_Create;
        private Telerik.WinControls.UI.RadPanel rpVIES_Results;
        private Telerik.WinControls.UI.RadGridView rgvVIES_Results;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem1;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem2;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
        private System.Windows.Forms.Label lblFlow;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbDispatch;
        private System.Windows.Forms.RadioButton rbArrival;
        private System.Windows.Forms.Button btnViewVatTransactions;
    }
}

